#ifndef ANALYSE_TP1_SEUILLAGE_H
#define ANALYSE_TP1_SEUILLAGE_H

#include <set>
#include "cv.hpp"
#include "convolution.h"

using namespace cv;

Mat seuillage_fixe(Mat & src, double seuil) {
    Size src_size = src.size();
    Mat res = Mat::zeros(src_size, CV_64F);
    Mat norme = detection_contours_multidirectionnel(src)[4];
    for (int i = 0; i < src_size.height; i++) {    // rows
        for (int j = 0; j < src_size.width; j++) { // columns
            if (norme.at<double>(i, j) >= seuil) res.at<double>(i, j) = 1.0;
        }
    }
    return res;
}

Mat seuillage_global(Mat & src) {
    Size src_size = src.size();
    Mat res = Mat::zeros(src_size, CV_64F);
    Mat normes = detection_contours_multidirectionnel(src)[4];

    float somme_normes = 0;

    for (int i = 0; i < src_size.height; i++) {    // rows
        for (int j = 0; j < src_size.width; j++) { // columns
            somme_normes += normes.at<double>(i, j);
        }
    }

    double seuil = somme_normes / (src_size.height * src_size.width);

    for (int i = 0; i < src_size.height; i++) {    // rows
        for (int j = 0; j < src_size.width; j++) { // columns
            if (normes.at<double>(i, j) >= seuil) res.at<double>(i, j) = 1.0;
        }
    }
    return res;
}

Mat seuillage_local(Mat & src, int taille_fenetre) {
    Size src_size = src.size();
    Mat res = Mat::zeros(src_size, CV_64F);
    Mat normes = detection_contours_multidirectionnel(src)[4];

    int t = taille_fenetre / 2;

    for (int i = t; i < src_size.height - t; i++) {    // rows
        for (int j = t; j < src_size.width - t; j++) { // columns
            double somme_normes = 0;

            for (int k = i - t; k < i + t; k++)     // rows
                for (int l = j - t; l < j + t; l++) // columns
                    somme_normes += normes.at<double>(k, l);

            double seuil = somme_normes / (taille_fenetre * taille_fenetre);

            if (normes.at<double>(i, j) >= seuil && somme_normes - 0.005 > 0) res.at<double>(i, j) = 1.0;
        }
    }
    return res;
}

Mat seuillage_local_bis(Mat & src, int taille_fenetre) {
    Size src_size = src.size();
    Mat res = Mat::zeros(src_size, CV_64F);
    Mat normes = detection_contours_multidirectionnel(src)[4];

    int size_x = src_size.height / taille_fenetre;
    if (size_x * taille_fenetre != src_size.height) size_x++;
    int size_y = src_size.width / taille_fenetre;
    if (size_y * taille_fenetre != src_size.width) size_y++;

    std::vector<double> seuils(taille_fenetre*taille_fenetre);

    for (int i = 0; i < src_size.height; i++) {    // rows
        for (int j = 0; j < src_size.width; j++) { // columns
            int position = int(i / size_x) * taille_fenetre + int(j / size_y);
            seuils[position] += normes.at<double>(i, j);
        }
    }

    for (auto & seuil : seuils) {
        seuil /= size_x*size_y;
    }

    for (int i = 0; i < src_size.height; i++) {    // rows
        for (int j = 0; j < src_size.width; j++) { // columns
            int position = int(i / size_x) * taille_fenetre + int(j / size_y);
            if (normes.at<double>(i, j) >= seuils[position]) {
                res.at<double>(i, j) = 1.0;
            }
        }
    }
    return res;
}

struct comparePoints {
    bool operator()(const Point& a, const Point& b) {
        if (a.x < b.x) return true;
        else if (a.x > b.x) return false;
        else {
            return a.y < b.y;
        }
    }
};

Mat seuillage_hysteresis(Mat & src, double seuil_haut = -1, double seuil_bas = -1) {
    Size src_size = src.size();
    Mat res = Mat::zeros(src_size, CV_64F);
    Mat normes = detection_contours_multidirectionnel(src)[4];

    if (seuil_haut == -1 || seuil_bas == -1) {
        // calcul automatique du seuil haut et bas
    }

    std::vector<Point> sb_vertices;
    std::vector<Point> sh_vertices;


    for (int i = 0; i < src_size.height; i++) {    // rows
        for (int j = 0; j < src_size.width ; j++) { // columns

            if (normes.at<double>(i, j) >= seuil_haut) {
                res.at<double>(i, j) = 1.0;
                sh_vertices.emplace_back(i, j);
            }
            else if (normes.at<double>(i, j) >= seuil_bas) {
                sb_vertices.emplace_back(i, j);
                res.at<double>(i, j) = 0.5;
            }
        }
    }


    std::vector<Point> pts, new_pts;
    std::set<Point, comparePoints> seen;

    for (auto & p : sb_vertices) {
        seen.clear();
        pts.clear();
        pts.emplace_back(p);
        seen.emplace(p);

        while(!pts.empty()) {
            new_pts.clear();

            for (Point & point : pts) {

                for (int i = -1; i < 2; i++) {
                    for (int j = -1; j < 2; j++) {

                        Point p2(point.x + i, point.y + j);

                        if ((i != 0 || j != 0) && p2.x >= 0 && p2.x < src_size.height && p2.y >= 0 &&
                            p2.y < src_size.width
                            && seen.find(p2) == seen.end()) {
                            seen.emplace(p2);

                            if (res.at<double>(p2.x, p2.y) == 1.0) {
                                res.at<double>(point.x, point.y) = 1.0;
                            } else if (res.at<double>(p2.x, p2.y) == 0.5) {
                                new_pts.emplace_back(p2);
                            }
                        }
                    }
                }
            }

            std::swap(pts, new_pts);
        }
    }

    for (auto & p : sb_vertices) {
        if (res.at<double>(p.x, p.y) != 1.0) {
            res.at<double>(p.x, p.y) = 0.0;
        }
    }
    return res;
}

Mat affinage_contour(Mat seuillage, Mat &norme, Mat &argument) {
    Size src_size = seuillage.size();

    for (int i = 1; i < src_size.height - 1; i++) {    // rows
        for (int j = 1; j < src_size.width - 1; j++) { // columns

            if (seuillage.at<double>(i, j) == 1.0) {
                int k = int(argument.at<double>(i, j) / M_PI * 4.0);

                int x2 = i, y2 = j;

                if (k == 0) {
                    x2++;
                } else if (k == 2) {
                    x2++; y2++;
                } else if (k == 3) {
                    y2++;
                } else {
                    x2--; y2++;
                }

                if (
                    (seuillage.at<double>(x2, y2) && norme.at<double>(i, j) < norme.at<double>(x2, y2))) {
                    seuillage.at<double>(i, j) = 0.0;
                }
            }
        }
    }

    return seuillage;
}


#endif //ANALYSE_TP1_SEUILLAGE_H
