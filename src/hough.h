#ifndef ANALYSE_TP1_HOUGH_H
#define ANALYSE_TP1_HOUGH_H

#include "cv.hpp"
#include <list>
#include <iterator>
#include <iostream>

using namespace cv;

std::vector<Vec2f> hough_infinite_lines(Mat & src, float delta_theta, float min_theta, float max_theta, float seuil) {
    Size src_size = src.size();

    // calcul bornes
    int max_rho = cvRound(cvSqrt(src_size.width * src_size.width + src_size.height * src_size.height));
    int numtheta = cvRound((max_theta - min_theta) / delta_theta);

    // calcul angles en radian
    float angles[numtheta];
    for (int i = 0; i < numtheta; i++) {
        angles[i] = min_theta * (CV_PI / 180.0);
        min_theta += delta_theta;
    }

    // calcul distance depuis origine
    float rhos[max_rho*2];
    for (int i = 0; i < max_rho; i++) {
        rhos[i] = - max_rho + i;
        rhos[2*max_rho-1-i] = max_rho - i;
    }


    Mat accumulator  = Mat::zeros(2 * max_rho, numtheta, CV_32F);

    for (int i = 0; i < src_size.height; i++) {    // rows
        for (int j = 0; j < src_size.width; j++) { // columns
            if (src.at<float>(i, j) > 0) {
                for (int k = 0; k < numtheta; k++) {;
                    float r = j * cos(angles[k]) + i * sin(angles[k]);
                    accumulator.at<float>(int(r) + max_rho, k) += 1;
                }
            }
        }
    }

    // trouve la valeur max // TODO a faire dans le boucle du haut ?
    float max_value = 0;

    for (int i = 0; i < max_rho * 2; i++) {    // rows
        for (int j = 0; j < numtheta; j++) { // columns
            max_value = max(max_value, accumulator.at<float>(i, j));
        }
    }

    std::vector<Vec2f> lines;

    for (int i = 0; i < max_rho * 2; i++) {    // rows
        for (int j = 0; j < numtheta; j++) { // columns
            if (accumulator.at<float>(i, j) > max_value * (1 - seuil/100)) {
                lines.emplace_back(rhos[i], angles[j]);
            }
            accumulator.at<float>(i, j) /= max_value;
        }
    }

    return lines;
}

std::vector<int> thresholds_segmentation(unsigned int f, unsigned int g, std::vector<int> binary_line) {
    // extract runs
    std::vector<int> r;

    int start = -1;
    for (int i = 0; i < binary_line.size(); i++) {
        if (binary_line[i] != 0 && start == -1) {
            start = i;
        }
        if (binary_line[i] == 0 && start != -1) {
            r.push_back(start);
            r.push_back(i-1);
            start = -1;
        }
    }

    if (start != -1) {
        r.push_back(start);
        r.push_back(binary_line.size()-1);
    }

    if (r.empty()) return std::vector<int>();

    int l = r.size()/2;
    std::vector<int> segs;
    segs.push_back(r[0]);
    int i;

    for (i = 0; i < l-1; i++) {
        if (r[2*(i+1)] - r[2*i+1] >= g) {
            segs.push_back(r[2*i+1]);
            segs.push_back(r[2*(i+1)]);
        }
    }

    segs.push_back(r[r.size()-1]);

    i = 0;
    while(i < int(segs.size()/2)) {
        if (segs[2*i+1] - segs[2*i] < f) {
            auto it = segs.begin();
            if (i > 0) {
                std::advance(it, 2 * i);
                segs.erase(it);
                it = segs.begin();
                std::advance(it, 2 * i);
                segs.erase(it);
            } else {
                segs.erase(it);
                segs.erase(segs.begin());
            }
        } else {
            i++;
        }
    }

    return segs;
}


#endif //ANALYSE_TP1_HOUGH_H
