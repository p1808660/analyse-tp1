
#ifndef ANALYSE_TP1_CONVOLUTION_H
#define ANALYSE_TP1_CONVOLUTION_H

#include "cv.hpp"

using namespace cv;

Mat sobel_kernel() {
    Mat sobel(3, 3, CV_64F);
    sobel.at<double>(0, 0) = -1; sobel.at<double>(0, 1) = 0; sobel.at<double>(0, 2) = 1;
    sobel.at<double>(1, 0) = -1; sobel.at<double>(1, 1) = 0; sobel.at<double>(1, 2) = 1;
    sobel.at<double>(2, 0) = -1; sobel.at<double>(2, 1) = 0; sobel.at<double>(2, 2) = 1;
    return sobel;
}

Mat prewitt_kernel() {
    Mat prewitt(3, 3, CV_64F);
    prewitt.at<double>(0, 0) = -1; prewitt.at<double>(0, 1) = 0; prewitt.at<double>(0, 2) = 1;
    prewitt.at<double>(1, 0) = -2; prewitt.at<double>(1, 1) = 0; prewitt.at<double>(1, 2) = 2;
    prewitt.at<double>(2, 0) = -1; prewitt.at<double>(2, 1) = 0; prewitt.at<double>(2, 2) = 1;
    return prewitt;
}

Mat kirsch_kernel() {
    Mat kirsch(3, 3, CV_64F);
    kirsch.at<double>(0, 0) = -3; kirsch.at<double>(0, 1) = -3; kirsch.at<double>(0, 2) = 5;
    kirsch.at<double>(1, 0) = -3; kirsch.at<double>(1, 1) = 0;  kirsch.at<double>(1, 2) = 5;
    kirsch.at<double>(2, 0) = -3; kirsch.at<double>(2, 1) = -3; kirsch.at<double>(2, 2) = 5;
    return kirsch;
}

Mat gradient_kernel(bool horizontal, int coef = 1)  {
    Mat res(3, 3, CV_64F);
    res.at<double>(0, 0) = horizontal ? -1 : 1;
    res.at<double>(1, 0) = horizontal ? -coef : 0;
    res.at<double>(2, 0) = -1;

    res.at<double>(0, 1) = horizontal ? 0 : coef;
    res.at<double>(1, 1) = 0;
    res.at<double>(2, 1) = horizontal ? 0 : -coef;

    res.at<double>(0, 2) = 1;
    res.at<double>(1, 2) = horizontal ? coef : 0;
    res.at<double>(2, 2) = horizontal ? 1 : -1;

    return res;
}

/**
 *
 * @param horizontal true -> pi/3, false 2pi/3
 * @param coef
 * @return
 */
Mat gradient_diagonal_kernel(bool horizontal, int coef = 1)  {
    Mat res(3, 3, CV_64F);
    res.at<double>(0, 0) = horizontal ? 0 : coef;
    res.at<double>(1, 0) = horizontal ? -1 : 1;
    res.at<double>(2, 0) = horizontal ? -coef : 0;

    res.at<double>(0, 1) = 1;
    res.at<double>(1, 1) = 0;
    res.at<double>(2, 1) = -1;

    res.at<double>(0, 2) = horizontal ? coef : 0;
    res.at<double>(1, 2) = horizontal ? 1 : -1;
    res.at<double>(2, 2) = horizontal ? 0 : -coef;

    return res;
}

Mat convolve_2d(Mat & src, Mat & kernel) {
    Size src_size = src.size();
    Size kernel_size = kernel.size();
    int kernel_width_center = kernel_size.width % 2 == 0 ? kernel_size.width / 2 : kernel_size.width / 2;
    int kernel_height_center = kernel_size.height % 2 == 0 ? kernel_size.height / 2 : kernel_size.height / 2;

    Mat res = Mat::zeros(src_size.height, src_size.width, CV_64F);

    /*
    printf("Source size (%d, %d), Kernel size (%d, %d), Center kernel (%d, %d)\n",
            src_size.height, src_size.width, kernel_size.height, kernel_size.width,
            kernel_height_center, kernel_width_center);
    */

    double compute;

    for (int i = kernel_height_center; i < src_size.height - kernel_height_center; i++) {    // rows
        for (int j = kernel_width_center; j < src_size.width - kernel_width_center; j++) {   // columns

            compute = 0;

            for (int m = 0; m < kernel_size.height; m++) {
                for (int n = 0; n < kernel_size.width; n++) {
                    compute += src.at<double>(i + m - kernel_height_center, j + n - kernel_width_center)
                            * kernel.at<double>(m, n);
                }
            }

            res.at<double>(i, j) = compute;
        }
    }

    return res;
}

/**
 *
 * @param src
 * @param coef
 * @return 0: Gx, 1: Gy, 2: module, 3: pente
 */
std::vector<Mat> detection_contours_bidirectionnel(Mat & src, int coef = 1) {
    std::vector<Mat> res(4);

    Mat horizontal = gradient_kernel(true, coef);
    Mat vertical = gradient_kernel(false, coef);

    // compute Gx & Gy
    res[0] = convolve_2d(src, horizontal);
    res[1] = convolve_2d(src, vertical);

    // compute module
    cv::sqrt(res[0].mul(res[0]) + res[1].mul(res[1]), res[2]);

    // compute gradient
    res[3] = res[1] / res[0];
    Size size = src.size();
    for (int i = 0; i < size.height; i++) {
        for (int j = 0; j < size.width; j++) {
            res[3].at<double>(i, j) = atan(res[3].at<double>(i, j));
        }
    }

    return res;
}

/**
 *
 * @param src
 * @param coef
 * @return 0: G0, 1: G1, 2: G2, 3: G3, 4: amplitude, 5: pente
 */
std::vector<Mat> detection_contours_multidirectionnel(Mat & src, int coef = 1) {
    std::vector<Mat> res(6);

    Mat horizontal = gradient_kernel(true, coef);
    Mat vertical = gradient_kernel(false, coef);
    Mat diag_pi3 = gradient_diagonal_kernel(true, coef);
    Mat diag_3pi4 = gradient_diagonal_kernel(false, coef);

    // compute G0, G1, G2, G3
    res[0] = convolve_2d(src, horizontal);
    res[1] = convolve_2d(src, diag_pi3);
    res[2] = convolve_2d(src, vertical);
    res[3] = convolve_2d(src, diag_3pi4);

    Size size = src.size();

    // init mat
    res[4] = Mat::zeros(size, CV_64F);
    res[5] = Mat::zeros(size, CV_64F);

    // calcul amplitude & pente
    for (int i = 0; i < size.height; i++) {    // rows
        for (int j = 0; j < size.width; j++) {  // columns
            int k_retenu = -1;
            double val_retenue = DBL_MIN;
            for (int k = 0; k < 4; k++) {
                if (abs(res[k].at<double>(i, j)) > val_retenue) {
                    val_retenue = abs(res[k].at<double>(i, j));
                    k_retenu = k;
                }
            }

            // ajout amplitude
            res[4].at<double>(i, j) = val_retenue;

            // ajout pente
            res[5].at<double>(i, j) = k_retenu * M_PI / 4.0;
        }
    }
    return res;
}

#endif //ANALYSE_TP1_CONVOLUTION_H
