//opencv
#include "opencv2/imgcodecs.hpp"
#include "opencv2/imgproc.hpp"
#include <opencv2/highgui.hpp>
//C++
#include <iostream>

#include "convolution.h"
#include "seuillage.h"
#include "hough.h"
#include <chrono>

using namespace std::chrono;
using namespace cv;
using namespace std;

int seuil_slider = 10;
int canny_slider = 300;
const int seuil_max = 100;
const int canny_max = 500;
int f_slider = 7;
const int f_max = 100;
int g_slider = 5;
const int g_max = 100;

Mat seuillage, original;

void compute_hough_segment(int seuil, int f, int g) {
    Canny(original, seuillage, canny_slider, canny_slider * 3.0);

    Mat ones = Mat::ones(seuillage.size(), CV_32F);
    ones.copyTo(seuillage, seuillage);

    namedWindow("Canny", WINDOW_AUTOSIZE);
    imshow("Canny", seuillage);


    std::vector<Vec2f> lines = hough_infinite_lines(seuillage, 2, 0, 180, seuil);

    // compute segments
    Mat seuillage_dilate, original_copy;
    original.copyTo(original_copy);
    dilate(seuillage, seuillage_dilate, getStructuringElement(MORPH_RECT, Size(3, 3)));

    // affichage hough
    Mat zeros = Mat::zeros(seuillage.size(), CV_32F);

    for (size_t i = 0; i < lines.size(); i++) {
        float rho = lines[i][0], theta = lines[i][1];
        Point pt1, pt2;
        float a = cos(theta), b = sin(theta);
        float x0 = a * rho, y0 = b * rho;
        pt1.x = cvRound(x0 + 2000 * (-b));
        pt1.y = cvRound(y0 + 2000 * (a));
        pt2.x = cvRound(x0 - 2000 * (-b));
        pt2.y = cvRound(y0 - 2000 * (a));
        line(zeros, pt1, pt2, Scalar(255, 255, 255), 1, CV_AA);

        LineIterator iterator(seuillage_dilate, pt1, pt2, 8, true);
        int j, count = iterator.count;
        std::vector<int> line_data(count);
        std::vector<Point> line_pos(count);


        for (j = 0; j < count; j++, ++iterator) {
            line_pos[j] = iterator.pos();
            line_data[j] = seuillage_dilate.at<float>(line_pos[j]) > 0;
        }

        std::vector<int> segs = thresholds_segmentation(f, g, line_data);

        for (j = 0; j < segs.size(); j += 2) {
            Point pt1 = line_pos[segs[j]];
            Point pt2 = line_pos[segs[j + 1]];
            //line( seuillage, pt1, pt2, Scalar(255,255,255), 1, CV_AA);
            line(original_copy, pt1, pt2, Scalar(0, 255, 255), 1, CV_AA);
        }
    }

    namedWindow("Hough infinite line", WINDOW_AUTOSIZE);
    imshow("Hough infinite line", zeros);

    //namedWindow("Hough finite line 2", WINDOW_AUTOSIZE);
    //imshow("Hough finite line 2", seuillage);

    namedWindow("Result", WINDOW_AUTOSIZE);
    imshow("Result", original_copy);

}

static void on_trackbar(int, void *) {
    compute_hough_segment(seuil_slider, f_slider, g_slider);
}

static void on_trackbar_2(int, void *) {
    compute_hough_segment(seuil_slider, f_slider, g_slider);
}

static void on_trackbar_3(int, void *) {
    compute_hough_segment(seuil_slider, f_slider, g_slider);
}

int main(int argc, char *argv[]) {

    // build example
    Mat image = Mat::zeros(512, 512, CV_32F);

    if (argc <= 1) {
        image = imread("ExempleSimple.png", CV_LOAD_IMAGE_COLOR);
    } else {
        image = imread(argv[1]);
        if (image.empty()) {
            printf("File not found %s\n", argv[1]);
            return -1;
        }
    }

    //image = imread("Lena-Soderberg.jpg", CV_LOAD_IMAGE_COLOR);
    //image = imread("chicago-train.jpg", CV_LOAD_IMAGE_COLOR);
    //image = imread("ExempleSimple.png", CV_LOAD_IMAGE_COLOR);
    //image = imread("city.jpg", CV_LOAD_IMAGE_COLOR);

    original = image;

    // convert RGB image to gray
    cvtColor(image, image, CV_BGR2GRAY);
    // change values from 0-255 to 0.0-1.0
    image.convertTo(image, CV_32F, 1 / 255.0);

    namedWindow("Source window", WINDOW_AUTOSIZE);
    imshow("Source window", image);

    /*
    seuillage = Mat::zeros(150, 150, CV_64F);
    seuillage.at<double>(75, 75) = 1;
    seuillage.at<double>(50, 50) = 1;
    seuillage.at<double>(60, 25) = 1;

    line( seuillage, Point(15, 15), Point(130, 130), Scalar(255,255,255), 1, CV_AA);
    line( seuillage, Point(130, 15), Point(15, 130), Scalar(255,255,255), 1, CV_AA);
    */

    compute_hough_segment(0, 1, 1);

    char TrackbarName[50];
    sprintf(TrackbarName, "Canny seuil");
    createTrackbar(TrackbarName, "Result", &canny_slider, canny_max, on_trackbar);
    on_trackbar(seuil_slider, nullptr);


    sprintf(TrackbarName, "Seuil");
    createTrackbar(TrackbarName, "Result", &seuil_slider, seuil_max, on_trackbar);
    on_trackbar(seuil_slider, nullptr);

    sprintf(TrackbarName, "Min pixel to be considered as segment");
    createTrackbar(TrackbarName, "Result", &f_slider, f_max, on_trackbar_2);
    on_trackbar_2(f_slider, nullptr);

    sprintf(TrackbarName, "Fill gap of pixel");
    createTrackbar(TrackbarName, "Result", &g_slider, g_max, on_trackbar_3);
    on_trackbar_3(g_slider, nullptr);

    waitKey(0);

    return EXIT_SUCCESS;
}

